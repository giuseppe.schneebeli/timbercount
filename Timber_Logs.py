# -*- coding: utf-8 -*-
"""
@author: Giuseppe
"""

import timbercountFunctions as tcf
# import the necessary packages
import cv2
import numpy as np
# load the image, convert it to grayscale, and blur it
import rawpy
#set the working directory
#donner le chemin
#Pfad eingeben
import os
os.chdir("./Timber Logs")

for root, dirs, files in os.walk("./"):
    #Read image using opencv
    for name in files:
        with rawpy.imread(name) as raw:
            #rgb = raw.postprocess(use_auto_wb=False,user_wb=(1,1,1,1))
            rgb = raw.postprocess()
            #rgb = raw.postprocess(user_wb=raw.daylight_whitebalance)
        imgBGR = cv2.cvtColor(rgb, cv2.COLOR_RGB2BGR)
        imgGammaAdj= tcf.adjust_gamma(imgBGR)
        ##imgsimpcb = tcf.simplest_cb(imgGammaAdj,1)
        minBGR = np.array([11,32,65])
        maxBGR = np.array([169,226,256])
        maskBGR =cv2.inRange(imgGammaAdj,minBGR,maxBGR)
        #resultBGR = cv2.bitwise_and(imgGammaAdj, imgGammaAdj, mask = maskBGR)

        imgHSV = cv2.cvtColor(imgGammaAdj, cv2.COLOR_BGR2HSV)
        minHSV = np.array([8,93,60])
        maxHSV = np.array([25,220,256])
        maskHSV =cv2.inRange(imgHSV,minHSV,maxHSV)
        #resultHSV = cv2.bitwise_and(imgGammaAdj, imgGammaAdj, mask = maskHSV)

        imgYCB = cv2.cvtColor(imgGammaAdj, cv2.COLOR_BGR2YCrCb)
        minYCB = np.array([40,140,70])
        maxYCB = np.array([223,180,120])
        maskYCB =cv2.inRange(imgYCB,minYCB,maxYCB)
        #resultYCB = cv2.bitwise_and(imgGammaAdj, imgGammaAdj, mask = maskYCB)

        imgLAB = cv2.cvtColor(imgGammaAdj, cv2.COLOR_RGB2LAB)
        minLAB = np.array([50,105,73])
        maxLAB = np.array([220,143,110])
        maskLAB =cv2.inRange(imgLAB,minLAB,maxLAB)
        #resultLAB = cv2.bitwise_and(imgGammaAdj, imgGammaAdj, mask = maskLAB)
        maskLAB[np.where(maskYCB == 0)] = maskYCB[np.where(maskYCB == 0)]
        maskLAB[np.where(maskHSV == 0)] = maskHSV[np.where(maskHSV == 0)]
        maskLAB[np.where(maskBGR == 0)] = maskBGR[np.where(maskBGR == 0)]

        #Transform image to gray level
        imgray = cv2.cvtColor(imgGammaAdj, cv2.COLOR_BGR2GRAY)

        ################################################################
        #histogram equalization

        hist,bins = np.histogram(imgray.flatten(), 256, [0,256])

        cdf = hist.cumsum()
        cdf_normalized = cdf * hist.max()/ cdf.max()

        cdf_m = np.ma.masked_equal(cdf,0)
        cdf_m = (cdf_m - cdf_m.min())*255/(cdf_m.max()-cdf_m.min())

        #Replace the missing value ...
        cdf = np.ma.filled(cdf_m,0).astype('uint8')
        #information on this variable 
        type(cdf)
        cdf.size

        img_eq = cdf[imgray]
        #cv2.namedWindow("hist. equalized",cv2.WINDOW_NORMAL)
        #cv2.resizeWindow("hist. equalized", 800,800)
        #cv2.imshow("hist. equalized", img_eq)
        #cv2.waitKey(0)

        ##################################################################
        #Threshold (optional, to test)

        ret, thresh = cv2.threshold(imgray,90, 200, cv2.THRESH_BINARY_INV)
        #cv2.namedWindow("thresholded",cv2.WINDOW_NORMAL)
        #cv2.resizeWindow("thresholded", 800,800)
        #cv2.imshow("thresholded", thresh)
        #cv2.waitKey(0)

        ##################################################################
        #Preprocessing (Canny, Threshold,... to test)

        #img_blur = cv2.medianBlur(img_eq,7)
        img_blur = cv2.GaussianBlur(img_eq, (9, 9), 0)
        #cv2.namedWindow("Median Blur",cv2.WINDOW_NORMAL)
        #cv2.resizeWindow("Median Blur", 800,800)
        #cv2.imshow("Median Blur",img_blur)
        #cv2.waitKey(0)

        ##################################################################

        kernel = np.ones((12,12),np.uint8) #adapt kernel size to fully fill later in closing
        edges = cv2.Canny(img_blur, 50, 60)
        closing = cv2.morphologyEx(edges, cv2.MORPH_CLOSE, kernel)# Morphological transformation: closing creates an array with values btw. 0 and 255

        # Morphological transformation: closing creates an array with values btw. 0 and 255
        closing = cv2.bitwise_not(closing) #invert the image 
        #cv2.namedWindow("canny",cv2.WINDOW_NORMAL)
        #cv2.resizeWindow("canny", 800,800)
        #cv2.imshow("canny", edges)
        #cv2.waitKey(0) 
                     
        #cv2.namedWindow("closing",cv2.WINDOW_NORMAL)
        #cv2.resizeWindow("closing", 800,800)
        #cv2.imshow("closing", closing)
        #cv2.waitKey(0)

        # Apply to the original image
        imask = maskLAB==255  #index of all positions with zero 
        cuttingSurface = np.zeros_like(closing, np.uint8)
        cuttingSurface[imask] = closing[imask]

        ##################################################################
        #find contours

        contours, hierarchy = cv2.findContours(cuttingSurface.copy(), cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)

        ##################################################################
        #plot the contour

        cnt = contours[-1]
        draw_all = cv2.drawContours(imgGammaAdj.copy(), contours, -1, (0,255,0), 1)
        #cv2.namedWindow("all contours",cv2.WINDOW_NORMAL)
        #cv2.resizeWindow("all contours", 800,800)
        #cv2.imshow("all contours",draw_all) 
        #cv2.waitKey(0)

        ##################################################################
        #Clean up contours

        areas_unfiltered = []
        filtered_contours = []

        for i in range(len(contours)):
          area = cv2.contourArea(contours[i])
          areas_unfiltered.append(area)
          print (area)
          if (area > 15000) and (area < 190000):
            filtered_contours.append(contours[i])
        
        draw = cv2.drawContours(imgray.copy(), filtered_contours, -1, (255,255,0), 2)
        #cv2.namedWindow("filtered contours",cv2.WINDOW_NORMAL)
        #cv2.resizeWindow("filtered contours", 800,800)
        #cv2.imshow("filtered contours", draw) 
        #cv2.waitKey(0)

        ##################################################################
        #crop out inside of the contour (mask)
        height, width, channels = imgGammaAdj.shape
        black_img = np.zeros((height,width), np.uint8)
        mask = cv2.drawContours(black_img, filtered_contours, -1, (255,255,255), -1)

        cropped = cv2.bitwise_and(imgGammaAdj,imgGammaAdj,mask=mask)
        #cv2.namedWindow("mask",cv2.WINDOW_NORMAL)
        #cv2.resizeWindow("mask", 800,800)
        #cv2.imshow("mask",cropped) 
        #cv2.waitKey(0)

        ##################################################################
        #bounding rectangle (straight)
        for j in range(len(filtered_contours)):
          x,y,w,h = cv2.boundingRect(filtered_contours[j])
          img_rect = cv2.rectangle(imgGammaAdj,(x,y),(x+w,y+h),(0,255,0),2) #no angle of rectangle (straight)
    
        cv2.namedWindow("bounding rectangles",cv2.WINDOW_NORMAL)
        cv2.resizeWindow("bounding rectangles", 800,800)
        cv2.imshow("bounding rectangles",img_rect) 
        cv2.waitKey(0)

        #Show the image
        #cv2.namedWindow('Final',cv2.WINDOW_NORMAL)
        #cv2.resizeWindow('Final', 800,800)
        #cv2.imshow("Final",cuttingSurface)
        #key = cv2.waitKey(0)
        saveloc = "../Bilder/"
        try:
          os.chdir(saveloc)
        except OSError:
          os.mkdir(saveloc)
          os.chdir(saveloc)
        cv2.imwrite((name[:-4] + ".jpg"),img_rect)
        os.chdir("../Timber Logs")

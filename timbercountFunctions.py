import numpy as np
import cv2
from matplotlib import pyplot as plt
import os
import math
import sys

def adjust_gamma(image, gamma=1.0, gain=1.0):
    #O = I ^ (1 / G)
    #I : input image 
    #G : gamma value. 
    #The output image O is then scaled back to the range [0, 255]
    gamma = gamma if gamma > 0 else 0.1
    # build a lookup table mapping the pixel values [0, 255] to
    # their adjusted gamma values
    invGamma = 1.0 / gamma
    table = np.array([((i / 255.0) ** invGamma) * 255 * gain
        for i in np.arange(0, 256)])
    for j in np.arange(0,256):
        if(table[j]>255):
            table[j]=255
    table=table.astype("uint8")
    # Apply gamma correction using the lookup table
    # LUT: chage the value of each pixel accoriding to the table
    adjusted = cv2.LUT(image, table)
    cv2.putText(adjusted, "g={}".format(gamma), (10, 30),
    cv2.FONT_HERSHEY_SIMPLEX, 0.8, (0, 0, 255), 3)
    return adjusted

def falseColor(image):
    #For the fun : let's change the color of the image
    #Read the image as grayscale
    im_gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    #Apply another collor scale : COLORMAP_SUMMER,COLORMAP_JET, ...
    im_color = cv2.applyColorMap(im_gray, cv2.COLORMAP_RAINBOW)

    return im_color

def filter(image):
    #Construction dunoyau du filtre, ici de taille 3x3
    #kernel = np.ones((5,5),np.float32)/25
    kernel = np.array( [[0,-1,0],[-1,5,-1],[0,-1,0]] )
    #Qu'est-ce qui se passe si la taille du filtre est de 5x5 ou de 7x7?
    
    #Apply the filter
    #https://docs.opencv.org/2.4/modules/imgproc/doc/filtering.html#filter2d
    dst = cv2.filter2D(image,-1,kernel)
    return dst

def showImageAgain(i,image,title=""):
    i = i.reshape((image.shape[0],image.shape[1],image.shape[2]))
    m,M = i.min(), i.max()
    img_rescaled=(i - m) / (M - m)
    return img_rescaled
    
def showGrayImageAgain(i,image,title=""):
    i = i.reshape((image.shape[0],image.shape[1]))
    m,M = i.min(), i.max()
    img_rescaled=(i - m) / (M - m)
    return img_rescaled

def whitening(image):
    # tous les pixels sont just considérés ensemble
    img = image.reshape(-1, image.shape[0]*image.shape[1]*image.shape[2])

    #Soustraction de la moyenne
    # Zero-centre the data (this calculates the mean separately across pixels and colour channels)
    img = img - img.mean()

    #Apply normalization
    #[:,None] : reshape 
    img = img / np.sqrt((img ** 2).sum(axis=1))[:,None]
    # or img = img / np.std(img, axis=0)
    return showImageAgain(img,image,"")

def ZCA_whitening(image):
    #ZCA whitening ....................................
    #for single image with grayscale
    
    gray_image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    # tous les pixels sont just considérés ensemble
    gray_image = gray_image.reshape(-1, image.shape[0]*image.shape[1])
    gray_image = gray_image - gray_image.mean()
    gray_image = gray_image / np.std(gray_image)
    gray_image = gray_image.reshape((image.shape[0],image.shape[1]))
    
    #Compute the covariance of the image data
    cov = np.cov(gray_image, rowvar=True)   # cov is (N, N)
    # Singular value decomposition
    U,S,V = np.linalg.svd(cov)     # U is (N, N), S is (N,)
    # Build the ZCA matrix
    epsilon = 1e-5
    zca_matrix = np.dot(U, np.dot(np.diag(1.0/np.sqrt(S + epsilon)), U.T))
    # transform the image data       zca_matrix is (N,N)
    zca = np.dot(zca_matrix, gray_image)    # zca is (N, 3072)

    # Whitened image ............................
    m,M = zca.min(), zca.max()
    zca_rescale=(zca - m) / (M - m)
    cv2.imshow("Mean free and rescaled oevr 0 - 1 ", zca_rescale)

def equalisation(img,name):
    # get blue layer
    blue,green,red = cv2.split(img)
    channels = [blue,green,red]
    channelnames = ["blue","green","red"]
    for i, x in enumerate(channels):
        #Make histogram and cumulative distribution
        #Histogram with numpy --> sooooo simple !
        hist,bins = np.histogram(x.flatten(),256,[0,256])

        #Cumulative histgram
        cdf = hist.cumsum()
        #Normalization for plot
        cdf_normalized = cdf * hist.max()/ cdf.max()

        plt.plot(cdf_normalized, color = 'b')
        plt.hist(x.flatten(),256,[0,256], color = 'r')
        plt.xlim([0,256])
        plt.legend(('cdf','histogram'), loc = 'upper left')
        try:
          os.chdir("../histograms/")
        except OSError:
          os.mkdir("../histograms/")
          os.chdir("../histograms/")
        plt.savefig(name+"_"+channelnames[i]+".png")
        plt.clf()
        os.chdir("../Timber Logs/")

        #Masked arrays are arrays that may have missing or invalid entries. 
        #Mask concept from numpy to exclude 0
        cdf_m = np.ma.masked_equal(cdf,0)

        #Normalization between 0 and 1 and multiply by 255
        cdf_m = (cdf_m - cdf_m.min())*255/(cdf_m.max()-cdf_m.min())
        #Replace the missing value ...
        cdf_b = np.ma.filled(cdf_m,0).astype('uint8')

        print(" The cdf is of ", type(cdf_b), "and of size", int(cdf_b.size))


        # Apply this equalization to the current image 
        channels[i] = cdf_b[x]
    img2 =cv2.merge(channels)
    return(img2)


def apply_mask(matrix, mask, fill_value):
    masked = np.ma.array(matrix, mask=mask, fill_value=fill_value)
    return masked.filled()

def apply_threshold(matrix, low_value, high_value):
    low_mask = matrix < low_value
    matrix = apply_mask(matrix, low_mask, low_value)

    high_mask = matrix > high_value
    matrix = apply_mask(matrix, high_mask, high_value)

    return matrix

def simplest_cb(img, percent):
    assert img.shape[2] == 3
    assert percent > 0 and percent < 100

    half_percent = percent / 200.0

    channels = cv2.split(img)

    out_channels = []
    for channel in channels:
        assert len(channel.shape) == 2
        # find the low and high precentile values (based on the input percentile)
        height, width = channel.shape
        vec_size = width * height
        flat = channel.reshape(vec_size)

        assert len(flat.shape) == 1

        flat = np.sort(flat)

        n_cols = flat.shape[0]

        low_val  = flat[math.floor(n_cols * half_percent)]
        high_val = flat[math.ceil( n_cols * (1.0 - half_percent))]

        print("Lowval: ", low_val)
        print("Highval: ", high_val)

        # saturate below the low percentile and above the high percentile
        thresholded = apply_threshold(channel, low_val, high_val)
        # scale the channel
        normalized = cv2.normalize(thresholded, thresholded.copy(), 0, 255, cv2.NORM_MINMAX)
        out_channels.append(normalized)

    return cv2.merge(out_channels)
